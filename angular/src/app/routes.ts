import { AttendanceComponent } from './pages/attendance/attendance.component';
import { PositionComponent } from './pages/position/position.component';
import { TimeKeepingComponent } from './pages/time-keeping/time-keeping.component';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './_components/login/login.component';
import { AuthGuard } from './_guards/auth.guard';
import { EmployeeComponent } from './pages/employee/employee/employee.component';

const appRoutes: Routes = [
  { path: 'attendance', component: AttendanceComponent, canActivate: [AuthGuard] },
  { path: 'position', component: PositionComponent, canActivate: [AuthGuard] },
  { path: 'time-keeping', component: TimeKeepingComponent, canActivate: [AuthGuard] },
  { path: 'employee', component: EmployeeComponent, canActivate: [AuthGuard] },
  { path: '', component: TimeKeepingComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },

  // otherwise redirect to home
  { path: '**', redirectTo: '' },
];

export const routing = RouterModule.forRoot(appRoutes);
