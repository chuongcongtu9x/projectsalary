export class EmployeeInput {
  EmpId!: number;
  EmpName!: string;
  DepartmentId!: number;
  Sex!: boolean;
  Pregnant!: boolean;
}
