﻿export class Employee {
  Id: number;
  Username: string;
  Password: string;
  EmpName: string;
  Birthday: Date;
  PositionId: number;
  DepartmentId: number;
  CurrentWage: number;
  Sex: boolean;
  Pregnant: boolean;
  Token: string;
}
