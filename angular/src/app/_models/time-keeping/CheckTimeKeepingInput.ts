export class CheckTimeKeepingInput {
  EmpId!: number;
  WorkTime!: number;
  Pregnant!: boolean;
}
