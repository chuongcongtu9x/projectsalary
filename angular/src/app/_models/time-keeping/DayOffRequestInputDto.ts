export class DayOffRequestInputDto {
  EmpId: number;
  OffDate: Date;
  Note: string;
}
