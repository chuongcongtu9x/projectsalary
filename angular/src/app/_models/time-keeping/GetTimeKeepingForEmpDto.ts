export class GetTimeKeepingForEmpDto {
  TimeKeepingId: number;
  EmpId: number;
  Position: string;
  WorkDay: number;
  MonthWage: number;
  EmpName: string;
  Birthday: Date;
  Sex: boolean;
  Pregnant: string;
  Department: string;
}
