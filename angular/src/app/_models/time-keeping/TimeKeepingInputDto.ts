export class TimeKeepingInputDto {
  EmpId!: number;
  EmpName!: string
  EmpTypeId!: number;
  Month!: Date;
  PositionId!: number;
}
