export class TimeKeepingDetail {
  Id: number;
  TimeKeepingId: number;
  Work: number;
  Note!: string;
  WorkDate!: Date;
  CreationTime: Date;
  ModifyDate!: Date;
  IsDelete: boolean;
}
