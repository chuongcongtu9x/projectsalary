export class TimeKeeping {
  Id: number;
  EmpId: number;
  WorkDay: number;
  DayOff: number;
  CreationTime: Date;
  ModifyDate!: Date;
  IsDelete: boolean;
}
