import { TimeKeepingInputDto } from './../_models/time-keeping/TimeKeepingInputDto';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from 'rxjs/internal/Observable';
import { TimeKeepingDetailInput } from '../_models/time-keeping/TimeKeepingDetailInput';
import { CheckTimeKeepingInput } from '../_models/time-keeping/CheckTimeKeepingInput';
import { DayOffRequestInputDto } from '../_models/time-keeping/DayOffRequestInputDto';

@Injectable({
  providedIn: 'root',
})
export class TimeKeepingService {
  readonly APIUrl = 'http://localhost:60276';

  constructor(private http: HttpClient) { }

  httpOptxions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  getTimeKeepingForEmp(val: TimeKeepingInputDto): Observable<any[]> {
    return this.http.post<any>(this.APIUrl + '/get-time-keeping', val);
  }

  getTimeKeepingDetail(val: TimeKeepingDetailInput): Observable<any[]> {
    return this.http.post<any>(this.APIUrl + '/get-time-keeping-detail', val);
  }

  checkTimeKeepingStart(val: CheckTimeKeepingInput): Observable<any[]> {
    return this.http.post<any>(this.APIUrl + '/time-keeping-start', val);
  }

  getTimeKeepingDetailOfEmp(val: TimeKeepingInputDto): Observable<any[]> {
    return this.http.post<any>(this.APIUrl + '/time-keeping-emp', val);
  }

  dayOffRequestOfEmp(val: DayOffRequestInputDto): Observable<any[]> {
    return this.http.post<any>(this.APIUrl + '/day-off-request', val);
  }
}
