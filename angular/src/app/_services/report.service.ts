import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TimeKeepingDetailInput } from '../_models/time-keeping/TimeKeepingDetailInput';
import { TimeKeepingInputDto } from '../_models/time-keeping/TimeKeepingInputDto';

@Injectable({
  providedIn: 'root'
})
export class ReportService {
  readonly APIUrl = 'http://localhost:60276';

  constructor(private http: HttpClient) { }
  httpOptxions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
  };

  exportEmployeeReport(): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/export-employee');
  }

  exportEmpWageReport(val: TimeKeepingDetailInput): Observable<any[]> {
    return this.http.post<any>(this.APIUrl + '/export-emp-wage', val);
  }
}
