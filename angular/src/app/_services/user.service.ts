﻿import { Department } from './../_models/employee/department';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { EmployeeInput } from '../_models/employee/employee-input';
import { Employee } from '../_models/employee/employee';

@Injectable({ providedIn: 'root' })
export class UserService {
  readonly APIUrl = 'http://localhost:60276';

  constructor(private http: HttpClient) { }

  getEmployees(val: EmployeeInput): Observable<any[]> {
    return this.http.post<any>(this.APIUrl + '/employees', val);
  }

  registerEmployee(val: Employee) {
    return this.http.post(this.APIUrl + '/auth/register', val);
  }

  updateEmployee(val: Employee) {
    return this.http.post(this.APIUrl + '/update-employee', val);
  }

  deleteEmployee(val: Employee): Observable<any[]> {
    return this.http.post<any>(this.APIUrl + '/delete-employee', val);
  }

  getDepartment(): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/get-department');
  }

  createDepartment(val: Department) {
    return this.http.post(this.APIUrl + '/create-department', val);
  }

  updateDepartment(val: Department) {
    return this.http.post(this.APIUrl + '/update-department', val);
  }

  deleteDepartment(val: Department) {
    return this.http.post(this.APIUrl + '/delete-department', val);
  }

  getPositions() {
    return this.http.get<any>(this.APIUrl + '/get-position');
  }
}
