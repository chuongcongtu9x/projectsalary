import { ReportService } from './../../../_services/report.service';
import { DataFormatService } from './../../../_services/data-format.service';
import { Employee } from 'src/app/_models/employee/employee';
import { Component, OnInit, ViewChild } from '@angular/core';
import { PaginationParamsModel } from 'src/app/_components/shared/common/models/base.model';
import { EmployeeInput } from 'src/app/_models/employee/employee-input';
import { UserService } from 'src/app/_services/user.service';
import { ceil } from 'lodash';
import { CreateOrEditEmployeeComponent } from './create-or-edit-employee/create-or-edit-employee.component';
import * as moment from 'moment';
declare let alertify: any;
@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss'],
})
export class EmployeeComponent implements OnInit {
  @ViewChild('createOrEditEmployee', { static: true }) createOrEditEmployee: CreateOrEditEmployeeComponent;
  paginationParams: PaginationParamsModel;

  columnsDef;
  defaultColDef;
  rowData = [];
  pagedRowData = [];
  params: any;
  user: Employee = new Employee();
  selectedData: Employee = new Employee();
  sexList = [
    { label: 'Tất cả', value: undefined },
    { label: 'Nam', value: true },
    { label: 'Nữ', value: false }
  ];
  statusList = [
    { label: 'Tất cả', value: undefined },
    { label: 'Có', value: true },
    { label: 'Không', value: false }
  ];
  departments = [];
  empInput: EmployeeInput = new EmployeeInput();

  constructor(private _employeeService: UserService,
    private reportService: ReportService) {
    this.columnsDef = [
      {
        headerName: 'STT',
        field: 'stt',
        cellRenderer: (params) =>
          (this.paginationParams.pageNum - 1) * this.paginationParams.pageSize +
          params.rowIndex + 1,
      },
      {
        headerName: 'Mã nhân viên',
        field: 'Id',
      },
      {
        headerName: 'Tên nhân viên',
        field: 'EmpName',
      },
      {
        headerName: 'Ngày sinh',
        field: 'Birthday',
        valueGetter: (params) =>
          params.data.Birthday
            ? moment(params.data.Birthday).format('DD/MM/YYYY')
            : null,
      },
      {
        headerName: 'Giới tính',
        field: 'Sex',
        valueGetter: (params) => params.data.Sex ? 'Nam' : 'Nữ'
      },
      {
        headerName: 'Mang thai',
        field: 'Pregnant',
        valueGetter: (params) => params.data.Pregnant ? 'Có' : 'Không'
      },
      {
        headerName: 'Chức vụ',
        field: 'Position',
      },
      {
        headerName: 'Phòng ban',
        field: 'Department',
      },
      {
        headerName: 'Lương/Công',
        field: 'CurrentWage',
        valueGetter: params => params?.data && params?.data?.CurrentWage ? (Math.round(params.data.CurrentWage).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')) : '0'
      },
    ];

    this.defaultColDef = {
      flex: 1,
      resizable: true,
      suppressMenu: true,
      menuTabs: [],
      tooltipValueGetter: (t: any) => t.value,
      textFormatter: function (r) {
        if (r == null) return null;
        return r.toLowerCase();
      },
    };
  }

  ngOnInit() {
    this.paginationParams = { pageNum: 1, pageSize: 10, totalCount: 0 };
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    JSON.parse(localStorage.getItem('department'))?.map(e =>
      this.departments.push(Object.assign({ label: e.DepartmentName, value: e.Id })));
    this.departments.unshift({ label: 'Tất cả', value: 0 });
    this.initialValue();
  }

  initialValue() {
    this.empInput = new EmployeeInput();
    this.empInput.DepartmentId = 0;
    this.empInput.Pregnant = undefined;
    this.empInput.Sex = undefined;
  }

  onSearch() {
    this.selectedData = new Employee();
    this.callBackEvent(this.params);
  }

  callBackEvent(event) {
    this.params = event;
    this.empInput.DepartmentId = Number(this.empInput.DepartmentId);
    this._employeeService.getEmployees(this.empInput).subscribe((res) => {
      this.rowData = res;
      this.pagedRowData =
        this.rowData.length > 0
          ? this.rowData.slice(
            (this.paginationParams.pageNum - 1) *
            this.paginationParams.pageSize,
            this.paginationParams.pageNum * this.paginationParams.pageSize
          )
          : [];
      this.paginationParams.totalCount = this.rowData.length;
      this.paginationParams.totalPage = ceil(
        this.rowData.length / this.paginationParams.pageSize
      );
      this.paginationParams.pageNum = 1;
    });
  }

  changePaginationParams(paginationParams: PaginationParamsModel) {
    this.paginationParams = paginationParams;
    this.paginationParams.skipCount =
      (paginationParams.pageNum - 1) * paginationParams.pageSize;
    this.paginationParams.pageSize = paginationParams.pageSize;

    this._employeeService.getEmployees(this.empInput).subscribe((res) => {
      this.rowData = res;
      this.pagedRowData = this.rowData
        ? this.rowData.slice(
          this.paginationParams.skipCount,
          this.paginationParams.pageNum * this.paginationParams.pageSize
        )
        : [];
    });
    this.params.api?.setRowData(this.pagedRowData);
  }

  onChangeSelection(params) {
    const selectedData = params.api.getSelectedRows();
    if (selectedData) this.selectedData = selectedData[0] ?? new Employee();
  }

  getVehicleCard() {
    this.callBackEvent(this.params);
  }

  add() {
    this.createOrEditEmployee.show(new Employee());
  }

  edit() {
    this.createOrEditEmployee.show(this.selectedData);
  }

  delete() {
    this._employeeService
      .deleteEmployee(this.selectedData)
      .subscribe((res) => {
        if (res) {
          alertify.success('Xóa nhân viên thành công');
          this.callBackEvent(this.params);
        } else alertify.success('Xóa nhân viên thất bại');
      });
  }

  export() {
    this.reportService.exportEmployeeReport().subscribe(blob => {
      alertify.success('Xuất thành công!')
    });
  }
}
