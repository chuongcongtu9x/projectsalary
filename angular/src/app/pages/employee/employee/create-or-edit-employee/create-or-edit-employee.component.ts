import { Position } from './../../../../_models/employee/position';
import { UserService } from 'src/app/_services/user.service';
import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Employee } from 'src/app/_models/employee/employee';
declare let alertify: any;

@Component({
  selector: 'app-create-or-edit-employee',
  templateUrl: './create-or-edit-employee.component.html',
  styleUrls: ['./create-or-edit-employee.component.scss']
})
export class CreateOrEditEmployeeComponent implements OnInit {
  @ViewChild('modal') public modal: ModalDirective;
  @Output('modalSave') modalSave = new EventEmitter();
  @Input() user: Employee;
  employee: Employee = new Employee();
  positions: any[] = [];
  departments: any[] = [];
  confirmPass: string;
  password: string;

  sexItems = [
    { label: 'Nam', value: true },
    { label: 'Nữ', value: false },
  ];

  pregnants = [
    { label: 'Có', value: true },
    { label: 'Không', value: false },
  ];

  constructor(
    private userService: UserService
  ) { }

  ngOnInit() {
  }

  hide() {
    this.modal.hide();
  }

  show(event?: Employee) {
    this.positions = [];
    this.departments = [];
    this.password = '';
    this.confirmPass = '';
    JSON.parse(localStorage.getItem('department'))?.map(e =>
      this.departments.push(Object.assign({ label: e.DepartmentName, value: e.Id })));
    JSON.parse(localStorage.getItem('position'))?.map(e =>
      this.positions.push(Object.assign({ label: e.PositionName, value: e.Id })));
    this.employee = new Employee();
    if (event?.Id) {
      this.employee = event;
      this.employee.Birthday = new Date(event.Birthday);
      if (this.employee.Id === this.user.Id) this.password = this.employee.Password;
    }
    this.modal.show();
  }

  createOrEdit() {
    if (!this.checkValidate()) return;
    if (this.employee?.Id) {
      this.userService.updateEmployee(this.employee).subscribe(res => {
        if (res) {
          alertify.success('Cập nhật thành công!');
          this.modalSave.emit();
          this.modal.hide()
          return;
        }
        alertify.error('Cập nhật thất bại!');
      })
    } else {
      this.employee.Password = this.password;
      this.userService.registerEmployee(this.employee).subscribe(res => {
        if (res) {
          alertify.success('Tạo nhân viên thành công!');
          this.modalSave.emit();
          this.modal.hide()
          return;
        }
        alertify.error('Tạo nhân viên thất bại!');
      });
    }
  }

  checkValidate() {
    if (!this.employee?.EmpName || this.employee?.EmpName === '') {
      alertify.error('Tên nhân viên không được trống');
      return false;
    }
    if (this.employee?.Sex == undefined) {
      alertify.error('Giới tính không được trống');
      return false;
    }
    if (!this.employee?.PositionId) {
      alertify.error('Yêu cầu chọn chức vụ');
      return false;
    }
    if (!this.employee?.DepartmentId) {
      alertify.error('Yêu cầu chọn phòng ban');
      return false;
    }
    if (this.employee?.Pregnant == undefined) {
      alertify.error('Trạng thái mang thai không được trống');
      return false;
    }
    if (!this.employee?.Birthday) {
      alertify.error('Ngày sinh không được trống');
      return false;
    }
    if (!this.employee?.Id && (!this.password || this.password === '')) {
      alertify.error('Mật khẩu không được trống');
      return false;
    }
    if (!this.employee?.Id && (!this.confirmPass || this.confirmPass === '' || this.password !== this.confirmPass)) {
      alertify.error('Yêu cầu xác nhận lại mật khẩu!');
      return false;
    }
    return true;
  }
}
