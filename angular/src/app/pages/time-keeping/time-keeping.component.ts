import { Position } from './../../_models/employee/position';
import { ceil } from 'lodash';
import { TimeKeepingInputDto } from './../../_models/time-keeping/TimeKeepingInputDto';
import { TimeKeepingService } from './../../_services/time-keeping.service';
import { TimeKeepingDetailComponent } from './time-keeping-detail/time-keeping-detail.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { PaginationParamsModel } from 'src/app/_components/shared/common/models/base.model';
import { AgCellButtonRendererComponent } from 'src/app/_components/ag-cell-button-renderer/ag-cell-button-renderer.component';
import { GetTimeKeepingForEmpDto } from 'src/app/_models/time-keeping/GetTimeKeepingForEmpDto';
declare let alertify: any;
@Component({
  selector: 'app-time-keeping',
  templateUrl: './time-keeping.component.html',
  styleUrls: ['./time-keeping.component.scss']
})
export class TimeKeepingComponent implements OnInit {
  @ViewChild('timeKeepingDetail', { static: false }) timeKeepingDetail: TimeKeepingDetailComponent;
  paginationParams: PaginationParamsModel;
  frameworkComponents;
  departments = [];
  timeKeepingInput: TimeKeepingInputDto = new TimeKeepingInputDto();
  selectedData: GetTimeKeepingForEmpDto = new GetTimeKeepingForEmpDto();

  empCode;
  empName;
  department: number = 0;
  month: Date;
  user;

  columnDefs;
  defaultColDef;
  rowData = [];
  pagedRowData: any[] = [];
  positions = [];
  position: number = 0;

  params: any;
  rowDataStorage: any[];
  constructor(
    private _timeKeepingService: TimeKeepingService,
  ) {
    this.columnDefs = [
      {
        headerName: 'STT',
        field: 'stt',
        valueGetter: hashValueGetter,
        flex: 0.5,
      },
      {
        headerName: 'Mã nhân viên',
        field: 'EmpId',
      },
      {
        headerName: 'Tên nhân viên',
        field: 'EmpName',
      },
      {
        headerName: 'Giới tính',
        field: 'Sex',
        valueGetter: params => params.data?.Sex ? 'Nam' : 'Nữ',
      },
      {
        headerName: 'Chức vụ',
        field: 'Position',
      },
      {
        headerName: 'Phòng ban',
        field: 'Department',
      },
      {
        headerName: 'Tổng số công',
        field: 'WorkDay',
        valueGetter: params => params.data?.WorkDay ? parseFloat(params.data?.WorkDay).toFixed(2) : 0,
      },
      {
        headerName: 'Lương tháng',
        field: 'MonthWage',
        valueGetter: params => params?.data && params?.data?.MonthWage ? (Math.round(params.data.MonthWage).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')) : '0'
      },
      {
        headerName: 'Actions',
        field: 'actions',
        cellRenderer: 'agInCellButtonComponent',
        cellClass: ['text-center', 'cell-border'],
        buttonDef: {
          text: 'Chi tiết',
          useRowData: true,
          function: this.onBtnClick.bind(this),
        }
      }
    ];
    this.defaultColDef = {
      flex: 1,
      resizable: true,
      suppressMenu: true,
      menuTabs: [],
      tooltipValueGetter: (t: any) => t.value,
      textFormatter: function (r) {
        if (r == null) return null;
        return r.toLowerCase();
      },
    };
  }

  ngOnInit() {
    this.frameworkComponents = {
      agInCellButtonComponent: AgCellButtonRendererComponent,
    };
    this.paginationParams = {
      pageNum: 1,
      pageSize: 10,
      totalCount: 0,
      totalPage: 1,
    };

    this.user = JSON.parse(localStorage.getItem('currentUser'));
    JSON.parse(localStorage.getItem('empType'))?.map(e =>
      this.departments.push(Object.assign({ label: e.Department, value: e.Id })));
    this.departments.unshift({ label: 'Tất cả', value: 0 });
    JSON.parse(localStorage.getItem('position'))?.map(e =>
      this.positions.push(Object.assign({ label: e.PositionName, value: e.Id, wage: e.Wage })));
      this.positions.unshift({ label: 'Tất cả', value: 0 });
  }

  onBtnClick(params) {
    this.timeKeepingDetail.show(this.selectedData, this.month);
  }

  callBackEvent(event) {
    this.params = event;
    this.onGridReady();
  }

  onGridReady() {
    this.timeKeepingInput = new TimeKeepingInputDto();
    this.timeKeepingInput.EmpId = this.empCode;
    this.timeKeepingInput.EmpName = this.empName;
    this.timeKeepingInput.Month = this.month;
    this.timeKeepingInput.PositionId = this.position;
    this.timeKeepingInput.EmpTypeId = Number(this.department);
    this._timeKeepingService
      .getTimeKeepingForEmp(this.timeKeepingInput)
      .subscribe((res) => {
        this.rowDataStorage = res;
        this.rowData = this.rowDataStorage;

        this.pagedRowData =
          this.rowData.length > 0
            ? this.rowData.slice(
              (this.paginationParams.pageNum - 1) *
              this.paginationParams.pageSize,
              this.paginationParams.pageNum * this.paginationParams.pageSize
            )
            : [];

        this.paginationParams.totalCount = this.rowData.length;
        this.paginationParams.totalPage = ceil(
          this.rowData.length / this.paginationParams.pageSize
        );
        this.paginationParams.pageNum = 1;
      });
  }

  changePaginationParams(paginationParams: PaginationParamsModel) {
    this.paginationParams = paginationParams;
    this.paginationParams.skipCount =
      (paginationParams.pageNum - 1) * paginationParams.pageSize;
    this.paginationParams.pageSize = paginationParams.pageSize;

    this.pagedRowData = this.rowData
      ? this.rowData.slice(
        this.paginationParams.skipCount,
        this.paginationParams.pageNum * this.paginationParams.pageSize
      )
      : [];
    this.params.api.setRowData(this.pagedRowData);
  }

  onChangeSelection(params) {
    const selectedData = params.api.getSelectedRows()[0] ?? new GetTimeKeepingForEmpDto();
    if (selectedData?.TimeKeepingId) this.selectedData = selectedData;
  }
}

var hashValueGetter = function (params) {
  return params.node.rowIndex + 1;
};
