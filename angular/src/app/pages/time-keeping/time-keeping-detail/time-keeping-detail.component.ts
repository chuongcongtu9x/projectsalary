import { ReportService } from './../../../_services/report.service';
import { Position } from './../../../_models/employee/position';
import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import * as moment from 'moment';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GetTimeKeepingForEmpDto } from 'src/app/_models/time-keeping/GetTimeKeepingForEmpDto';
import { TimeKeepingDetailInput } from 'src/app/_models/time-keeping/TimeKeepingDetailInput';
import { TimeKeepingService } from 'src/app/_services/time-keeping.service';
import { TimeKeepingInputDto } from 'src/app/_models/time-keeping/TimeKeepingInputDto';
declare let alertify: any;

@Component({
  selector: 'app-time-keeping-detail',
  templateUrl: './time-keeping-detail.component.html',
  styleUrls: ['./time-keeping-detail.component.scss']
})
export class TimeKeepingDetailComponent implements OnInit {
  @ViewChild('modal') public modal: ModalDirective;
  @Output() modalSave = new EventEmitter();
  timeKeepingColDef;
  defaultColDef;
  addNewParams;
  month?: Date;
  selectedData: GetTimeKeepingForEmpDto = new GetTimeKeepingForEmpDto();
  timeKeepingDetails = [];
  departments = [];
  positions = [];
  monthWage: string = '0'
  sexList = [
    { label: 'Nam', value: true },
    { label: 'Nữ', value: false }
  ];
  statusList = [
    { label: 'Có', value: true },
    { label: 'Không', value: false }
  ];

  constructor(
    private _timeKeepingService: TimeKeepingService,
    private reportService: ReportService
  ) {
    this.timeKeepingColDef = [
      {
        headerName: 'STT',
        field: 'stt',
        valueGetter: this.hashValueGetter,
        flex: 0.5,
      },
      {
        headerName: 'Ngày làm',
        field: 'WorkDate',
        valueGetter: (params) => params?.data && params?.data?.WorkDate ?
          moment(params.data.WorkDate).format('DD/MM/YYYY') : '',
      },
      {
        headerName: 'Ngày nghỉ',
        field: 'OffDate',
        valueGetter: (params) => params?.data && params?.data?.OffDate ?
          moment(params.data.OffDate).format('DD/MM/YYYY') : '',
      },
      {
        headerName: 'Số công',
        field: 'Work',
      },
      {
        headerName: 'Ghi chú',
        field: 'Note',
      }
    ];
    this.defaultColDef = {
      flex: 1,
      resizable: true,
      suppressMenu: true,
      menuTabs: [],
      tooltipValueGetter: (t: any) => t.value,
      textFormatter: function (r) {
        if (r == null) return null;
        return r.toLowerCase();
      },
    };
  }

  ngOnInit(): void {
    JSON.parse(localStorage.getItem('department'))?.map(e =>
      this.departments.push(Object.assign({ label: e.DepartmentName, value: e.Id })));
    JSON.parse(localStorage.getItem('position'))?.map(e =>
      this.positions.push(Object.assign({ label: e.PositionName, value: e.Id })));
  }

  show(selected?: GetTimeKeepingForEmpDto, month?: Date) {
    this.selectedData = selected ?? new GetTimeKeepingForEmpDto();
    this.month = month;
    this.selectedData.Birthday = new Date(this.selectedData.Birthday);
    this.monthWage = this.selectedData?.MonthWage ? Math.round(this.selectedData.MonthWage).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,') : '0';
    this.onGridReady();
    this.modal.show();
  }

  callBackEventAddNew(params) {
    this.addNewParams = params;
  }

  onGridReady() {
    if (this.selectedData?.TimeKeepingId) {
      var timeKeepingInput = new TimeKeepingDetailInput();
      timeKeepingInput.TimeKeepingId = this.selectedData.TimeKeepingId;
      this._timeKeepingService.getTimeKeepingDetail(timeKeepingInput).subscribe(res => {
        this.timeKeepingDetails = res ?? [];
      });
    }
  }

  hide() {
    this.modal.hide();
  }

  hashValueGetter = function (params) {
    return params.node.rowIndex + 1;
  };

  export() {
    var timeKeepingInput = new TimeKeepingDetailInput();

    timeKeepingInput.TimeKeepingId = this.selectedData.TimeKeepingId;
    timeKeepingInput.Month = this.month ?? new Date();
    this.reportService.exportEmpWageReport(timeKeepingInput).subscribe(blob => {
      alertify.success('Xuất thành công!')
    });
  }

}
