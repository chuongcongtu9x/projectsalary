import { Department } from './../../../_models/employee/department';
import { UserService } from 'src/app/_services/user.service';
import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
declare let alertify: any;

@Component({
  selector: 'app-create-or-edit-position',
  templateUrl: './create-or-edit-position.component.html',
  styleUrls: ['./create-or-edit-position.component.scss']
})
export class CreateOrEditPositionComponent implements OnInit {
  @ViewChild('modal') public modal: ModalDirective;
  @Output() modalSave = new EventEmitter();

  statuses = [{ label: 'Hoạt động', value: false }, { label: 'Ngừng hoạt động', value: true }]
  department: Department = new Department();
  constructor(
    private userService: UserService,
  ) { }

  ngOnInit() {
  }

  show(selectedData?) {
    this.department = new Department();
    this.department.IsDelete = false;
    if (selectedData) this.department = selectedData;
    this.modal.show();
  }

  hide() {
    this.modal.hide();
  }

  checkValidate() {
    if (!this.department.DepartmentName || this.department.IsDelete === undefined) return false;
    return true;
  }

  createOrEditEmpType() {
    if (!this.checkValidate()) return alertify.error('Yêu cầu nhập đủ thông tin!');
    if (this.department && this.department.Id) {
      this.userService.updateDepartment(this.department).subscribe(res => {
        if (res) {
          alertify.success('Cập nhật thành công!');
          localStorage.setItem("department", JSON.stringify(res));
          this.modalSave.emit();
          this.modal.hide()
          return;
        }
        alertify.error('Cập nhật thất bại!');
      });
    } else {
      this.userService.createDepartment(this.department).subscribe(res => {
        if (res) {
          alertify.success('Tạo phòng thành công!');
          localStorage.setItem("department", JSON.stringify(res));
          this.modalSave.emit();
          this.modal.hide()
          return;
        }
        alertify.error('Tạo phòng thất bại!');
      });
    }
  }

}
