import { CreateOrEditPositionComponent } from './create-or-edit-position/create-or-edit-position.component';
import { Component, OnInit, ViewChild } from '@angular/core';
import { PaginationParamsModel } from 'src/app/_components/shared/common/models/base.model';
import { UserService } from 'src/app/_services/user.service';
import { Department } from 'src/app/_models/employee/department';
declare let alertify: any;
@Component({
  selector: 'app-position',
  templateUrl: './position.component.html',
  styleUrls: ['./position.component.scss']
})
export class PositionComponent implements OnInit {
  @ViewChild('createOrEditPosition', { static: true }) createOrEditPosition: CreateOrEditPositionComponent;
  paginationParams: PaginationParamsModel;
  selectedData: Department = new Department();
  positionColumnDefs;
  defaultColDef;
  rowData = [];
  pagedRowData: any[];
  params: any;

  user: any;
  constructor(
    private userService: UserService
  ) {
    this.positionColumnDefs = [
      {
        headerName: 'STT',
        field: 'stt',
        cellRenderer: params => (this.paginationParams.pageNum - 1) * this.paginationParams.pageSize + params.rowIndex + 1,
      },
      {
        headerName: 'Mã phòng ban',
        field: 'Id',
      },
      {
        headerName: 'Tên phòng ban',
        field: 'DepartmentName',
      },
      {
        headerName: 'Trạng thái',
        field: 'IsDelete',
        valueGetter: (params) => !params.data?.IsDelete ? 'Hoạt động' : 'Không hoạt động'
      }
    ];

    this.defaultColDef = {
      flex: 1,
      resizable: true,
      suppressMenu: true,
      menuTabs: [],
      tooltipValueGetter: (t: any) => t.value,
      textFormatter: function (r) {
        if (r == null) return null;
        return r.toLowerCase();
      },
    };
  }

  ngOnInit() {
    this.paginationParams = { pageNum: 1, pageSize: 10, totalCount: 0 };
    this.user = JSON.parse(localStorage.getItem('currentUser'));
  }

  callBackEvent(event) {
    this.params = event;
    this.onGridReady();
  }

  onGridReady() {
    this.selectedData = new Department();
    this.userService.getDepartment().subscribe(res => {
      this.rowData = res;
      this.params.api.setRowData(this.rowData);
    });
  }

  create() {
    this.createOrEditPosition.show();
  }

  edit() {
    if (this.selectedData?.Id) {
      this.createOrEditPosition.show(this.selectedData);
    }
  }

  delete() {
    if (this.selectedData?.Id) {
      this.userService.deleteDepartment(this.selectedData).subscribe(res => {
        if (res) {
          alertify.success('Hủy phòng thành công!');
          this.onGridReady();
          return;
        }
        alertify.error('Hủy phòng thất bại!');
      });
    }
  }

  onChangeSelection(params) {
    const selectedData = params.api.getSelectedRows()[0] ?? new Department();
    if (selectedData && selectedData.Id) {
      this.selectedData = selectedData;
    }
  }

  closeModal() {
    this.selectedData = new Department();
  }

}
