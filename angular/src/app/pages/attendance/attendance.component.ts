import { Position } from './../../_models/employee/position';
import { Department } from 'src/app/_models/employee/department';
import { Employee } from './../../_models/employee/employee';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { PaginationParamsModel } from 'src/app/_components/shared/common/models/base.model';
import { CheckTimeKeepingInput } from 'src/app/_models/time-keeping/CheckTimeKeepingInput';
import { TimeKeepingService } from 'src/app/_services/time-keeping.service';
import { DayOffRequestInputDto } from 'src/app/_models/time-keeping/DayOffRequestInputDto';
import { TimeKeepingInputDto } from 'src/app/_models/time-keeping/TimeKeepingInputDto';
declare let alertify: any;

@Component({
  selector: 'app-attendance',
  templateUrl: './attendance.component.html',
  styleUrls: ['./attendance.component.scss']
})
export class AttendanceComponent implements OnInit {
  paginationParams: PaginationParamsModel;

  columnDefs;
  defaultColDef;
  pagedRowData = [];
  rowData = [];
  workDate = moment();
  offDate = moment();
  note!: string;
  params;
  dayOff: boolean = false;
  checkAttendance: boolean = true;

  currentUser: Employee = new Employee();
  sexList = [
    { label: 'Nam', value: true },
    { label: 'Nữ', value: false }
  ];
  statusList = [
    { label: 'Có', value: true },
    { label: 'Không', value: false }
  ];
  departments = [];
  positions = [];
  constructor(
    private _timeKeepingService: TimeKeepingService
  ) {
    this.columnDefs = [
      {
        headerName: 'STT',
        field: 'stt',
        valueGetter: hashValueGetter,
        flex: 0.5,
      },
      {
        headerName: 'Ngày làm',
        field: 'WorkDate',
        valueGetter: (params) => params?.data && params?.data?.WorkDate ?
          moment(params.data.WorkDate).format('DD/MM/YYYY') : '',
      },
      {
        headerName: 'Ngày nghỉ',
        field: 'OffDate',
        valueGetter: (params) => params?.data && params?.data?.OffDate ?
          moment(params.data.OffDate).format('DD/MM/YYYY') : '',
      },
      {
        headerName: 'Số công',
        field: 'Work',
      },
      {
        headerName: 'Ghi chú',
        field: 'Note',
      }
    ];
    this.defaultColDef = {
      flex: 1,
      resizable: true,
      suppressMenu: true,
      menuTabs: [],
      tooltipValueGetter: (t: any) => t.value,
      textFormatter: function (r) {
        if (r == null) return null;
        return r.toLowerCase();
      },
    };
  }

  ngOnInit(): void {
    this.paginationParams = {
      pageNum: 1,
      pageSize: 10,
      totalCount: 0,
      totalPage: 1,
    };
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    JSON.parse(localStorage.getItem('department'))?.map(e =>
      this.departments.push(Object.assign({ label: e.DepartmentName, value: e.Id })));
    JSON.parse(localStorage.getItem('position'))?.map(e =>
      this.positions.push(Object.assign({ label: e.PositionName, value: e.Id })));
  }

  callBackEvent(event) {
    this.params = event;
    var input = new TimeKeepingInputDto();
    input.EmpId = this.currentUser.Id
    this._timeKeepingService.getTimeKeepingDetailOfEmp(input).subscribe(res => {
      this.pagedRowData = res;
    });
  }

  cancel() {
    this.dayOff = false;
  }

  checkDayOff() {
    this.dayOff = true;
    this.offDate = moment();
    this.note = "";
  }

  attendanceStart() {
    this.checkAttendance = !this.checkAttendance;
    var checkTimeKeepingInput = new CheckTimeKeepingInput();
    checkTimeKeepingInput.EmpId = this.currentUser.Id;
    checkTimeKeepingInput.Pregnant = this.currentUser.Pregnant;
    this._timeKeepingService.checkTimeKeepingStart(checkTimeKeepingInput).subscribe(res => {
      if (res) alertify.success('Chấm công thành công');
      else alertify.eror('Chấm công thất bại!');
      this.callBackEvent(this.params);
    });
  }

  submitDayOff() {
    var dayOffReuestInput = new DayOffRequestInputDto();
    dayOffReuestInput.EmpId = this.currentUser?.Id;
    dayOffReuestInput.OffDate = new Date(moment(this.offDate).format('YYYY/MM/DD'));
    dayOffReuestInput.Note = this.note;
    if (!this.offDate || !this.note || this.note == '') {
      alertify.error('Yêu cầu nhập đẩy đủ ngày xin nghỉ và lý do!');
      return;
    }
    let offDate = moment(this.offDate, 'YYYY/MM/DD');
    let currentDate = moment(new Date(), 'YYYY/MM/DD');
    if (Number(offDate.diff(currentDate, 'days')) < 3) {
      alertify.error('Tối thiểu phải xin nghỉ trước 3 ngày!');
      return;
    }
    this._timeKeepingService.dayOffRequestOfEmp(dayOffReuestInput).subscribe(res => {
      if (res) {
        alertify.success('Xin nghỉ thành công!');
        this.callBackEvent(this.params);
        return;
      }
      alertify.error('Xin nghỉ thất bại!');
    });

  }
}

var hashValueGetter = function (params) {
  return params.node.rowIndex + 1;
};
