import { ControlValueAccessor, NG_VALUE_ACCESSOR, NG_VALIDATORS, Validator, AbstractControl, ValidationErrors } from '@angular/forms';
import {
    Component,
    Input,
    Output,
    EventEmitter,
    forwardRef,
} from '@angular/core';

@Component({
    selector: 'tmss-textarea',
    templateUrl: './tmss-textarea.component.html',
    styleUrls: ['./tmss-textarea.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => TmssTextareaComponent),
            multi: true,
        },
        {
        provide: NG_VALIDATORS,
        useExisting: forwardRef(() => TmssTextareaComponent),
        multi: true
        }
    ],
})
export class TmssTextareaComponent implements ControlValueAccessor, Validator {
    @Input() className: string;
    @Input() addOnMinWidth: string;
    @Input() text: string;
    @Input() isRequired: boolean;
    @Input() placeholder: string = '';
    @Input() disable;
    @Input() value;
    @Input() rows;
    @Input() hideLeftText;
    @Input() height: string;

    style;

    // tslint:disable-next-line:no-output-on-prefix
    @Output() onChoose = new EventEmitter();
    // tslint:disable-next-line:ban-types
    private onChange: Function;
    @Input() isDisabled: boolean;

    constructor() {}
    validate(control: AbstractControl): ValidationErrors {
        if (!control.value || control.value === '') {
            return {
                isRequired: { valid: false, actual: control.value}
            };
        }

        return null;
    }
    registerOnValidatorChange?(fn: () => void): void {

    }

    // tslint:disable-next-line: use-lifecycle-interface
    ngOnInit(): void {
        //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
        //Add 'implements OnInit' to the class.
        if (this.height) {
            this.setHeight(this.height);
        }
    }

    writeValue(obj: any): void {
        this.value = obj || '';
    }
    registerOnChange(fn: any): void {
        this.onChange = fn;
    }
    registerOnTouched(fn: any): void {}
    setDisabledState?(isDisabled: boolean): void {
        this.isDisabled = isDisabled;
    }

    onBlur(event?) {
        const value = event ? (event.target as HTMLInputElement).value : null;
        if (!value || value === '') {
            this.value = null;
        }

        this.onValueChange(event);

        this.validate(event.target.value);
    }

    onValueChange(e) {
        this.value = e ? e.target.value : '';
        if (typeof this.onChange === 'function') {
            this.onChange(e.target.value);
        }
    }

    setHeight(height) {
        this.style = Object.assign({}, { height });
    }
}
