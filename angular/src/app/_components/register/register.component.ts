﻿import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { first, map } from 'rxjs/operators';
import { AuthenticationService } from '../../_services/authentication.service';
import { UserService } from '../../_services/user.service';
import { AlertService } from '../../_services/alert.service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { Employee } from 'src/app/_models/employee/employee';
declare let alertify: any;

@Component({
  selector: 'app-register',
  templateUrl: 'register.component.html'
})
export class RegisterComponent implements OnInit {
  @ViewChild('modal') public modal: ModalDirective;
  @Output() close = new EventEmitter();

  employee: Employee = new Employee();
  confirmPass: string;
  departments: any[] = [];
  sexItems = [
    { label: 'Nam', value: true },
    { label: 'Nữ', value: false },
  ];
  positions = [];
  pregnants = [
    { label: 'Có', value: true },
    { label: 'Không', value: false },
  ];

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private alertService: AlertService
  ) {
    // if (this.authenticationService.currentUserValue) {
    //   this.router.navigate(['/']);
    // }
  }

  ngOnInit() { }

  show(event?) {
    this.departments = [];
    this.positions = [];
    JSON.parse(localStorage.getItem('department'))?.map(e =>
      this.departments.push(Object.assign({ label: e.DepartmentName, value: e.Id })));
    JSON.parse(localStorage.getItem('position'))?.map(e =>
      this.positions.push(Object.assign({ label: e.PositionName, value: e.Id, wage: e.Wage })));
    this.modal.show();
  }

  hide() {
    this.modal.hide();
  }

  checkValidate() {
    if (this.employee.Password !== this.confirmPass) {
      alertify.error('Yêu cầu xác nhận lại mật khẩu!');
      return false;
    }

    if (!this.employee.Username || !this.employee.Password || !this.employee.EmpName || !this.employee.DepartmentId || !this.employee.PositionId
      || this.employee.Sex == undefined || this.employee.Pregnant == undefined || !this.employee.Birthday) {
      alertify.error(('Yêu cầu nhập đầy đủ thông tin!'));
      return false;
    }

    return true;
  }

  save() {
    if (!this.checkValidate()) return;
    this.employee.CurrentWage = this.positions.find(e => e.value === Number(this.employee.PositionId))?.wage;
    this.userService
      .registerEmployee(this.employee)
      .pipe(first())
      .subscribe(
        (data) => {
          if (data) {
            this.alertService.success('Registration successful', true);
            alertify.success('Đăng ký thành công');
            this.close.emit(this.employee);
            this.modal.hide();
            // this.router.navigate(['/login']);
          } else {
            alertify.error('Tài khoản đã tồn tại!');
            return;
          }
        },
        (error) => {
          this.alertService.error(error);
        }
      );
  }
}
