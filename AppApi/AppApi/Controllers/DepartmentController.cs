﻿using AppApi.DL;
using AppApi.Entities.Department;
using System;
using System.Collections.Generic;
using System.Web.Http;

namespace AppApi.Controllers
{
    public class DepartmentController : ApiController
    {
        DepartmentDL department = new DepartmentDL();

        [HttpGet]
        [Route("get-position")]
        public List<Position> GetPositions()
        {
            try
            {
                return department.GetPositions();
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpGet]
        [Route("get-department")]
        public List<Department> GetDepartments()
        {
            try
            {
                return department.GetDepartments();
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        [Route("create-department")]
        public bool CreateDepartment(Department input)
        {
            try
            {
                return department.CreateDepartment(input);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        [Route("update-department")]
        public bool UpdateDepartment(Department input)
        {
            try
            {
                return department.UpdateDepartment(input);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        [Route("delete-department")]
        public bool DeleteDepartment(Department input)
        {
            try
            {
                return department.DeleteDepartment(input.Id);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}