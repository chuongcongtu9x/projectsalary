﻿using AppApi.DL;
using AppApi.Entities.TimeKeeping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace AppApi.Controllers
{
    public class TimeKeepingController : ApiController
    {
        TimeKeepingDL timeKeeping  = new TimeKeepingDL();
        [HttpPost]
        [Route("get-time-keeping")]
        public List<GetTimeKeepingForEmp> GetTimeKeepingForEmp(TimeKeepingInputDto input)
        {
            try
            {
                return timeKeeping.GetTimeKeepingForEmpDL(input);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        [Route("get-time-keeping-detail")]
        public List<TimeKeepingDetail> GetTimeKeepingDetail(TimeKeepingDetailInput input)
        {
            try
            {
                return timeKeeping.GetTimeKeepingDetailDL(input);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        [Route("time-keeping-start")]
        public bool CheckTimeKeepingStart(CheckTimeKeepingInput input)
        {
            try
            {
                return timeKeeping.CheckTimeKeepingStartDL(input);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        [Route("time-keeping-emp")]
        public List<TimeKeepingDetail> GetTimeKeepingDetailOfEmpDL(TimeKeepingInputDto input)
        {
            try
            {
                return timeKeeping.GetTimeKeepingDetailOfEmpDL(input);
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        [Route("day-off-request")]
        public bool DayOffRequestOfEmp(DayOffRequestInputDto input)
        {
            try
            {
                return timeKeeping.DayOffRequestOfEmp(input);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}