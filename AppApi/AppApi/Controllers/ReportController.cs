﻿using System.Data;
using System.IO;
using System.Web.Http;
using AppApi.DL;
using AppApi.Entities.TimeKeeping;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

namespace AppApi.Controllers
{
    public class ReportController : ApiController
    {
        [HttpGet]
        [Route("export-employee")]
        public void ExportEmployeeReport()
        {
            string FileName = "EmployeeReport.rpt";
            string FullPath = Path.Combine("ReportFolder", FileName);
            ReportDocument rptDoc = new ReportDocument();

            rptDoc.Load(System.Web.HttpContext.Current.Server.MapPath(FullPath));
            //rptDoc.SetDataSource(dt);
            //Export to PDF
            ExportOptions CrExportOptions;
            DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
            PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
            CrDiskFileDestinationOptions.DiskFileName = "E:/Temp/EmployeeReport.pdf"; // Tu config folder san co
            CrExportOptions = rptDoc.ExportOptions;
            CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
            CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
            CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
            CrExportOptions.FormatOptions = CrFormatTypeOptions;
            rptDoc.Export(CrExportOptions);
        }

        [HttpPost]
        [Route("export-emp-wage")]
        public void ExportEmpWageReport(TimeKeepingDetailInput input)
        {
            string FileName = "EmpWageReport.rpt";
            string FullPath = Path.Combine("ReportFolder", FileName);
            ReportDocument rptDoc = new ReportDocument();

            rptDoc.Load(System.Web.HttpContext.Current.Server.MapPath(FullPath));
            rptDoc.Refresh();           //Refresh Report to OnLoad new Data
            rptDoc.SetDatabaseLogon("sa", "chuongtq@1999"); // Log On DataBase
            rptDoc.SetParameterValue("@TIMEKEEPINGID", input.TimeKeepingId); // Input Parameter Value When search on Front end
            rptDoc.SetParameterValue("@MONTH", input.Month);
            //Export to PDF
            ExportOptions CrExportOptions;
            DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
            PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
            CrDiskFileDestinationOptions.DiskFileName = "E:/Temp/EmpWageReport.pdf"; // Tu config folder san co
            CrExportOptions = rptDoc.ExportOptions;
            CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
            CrExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
            CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
            CrExportOptions.FormatOptions = CrFormatTypeOptions;
            rptDoc.Export(CrExportOptions);
        }

    }
}