﻿namespace AppApi.Entities.Employee
{
    public class EmployeeInputDto
    {
        public string EmpName { get; set; }
        public int? EmpId { get; set; }
        public int? DepartmentId { get; set; }
        public bool? Sex { get; set; }
        public bool? Pregnant { get; set; }
    }
}
