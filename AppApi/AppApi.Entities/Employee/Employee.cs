﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AppApi.Entities.Employee
{
    public class Employee
    {
        public int Id { get; set; }
        [StringLength(50)]
        public string EmpName { get; set; }
        public DateTime? Birthday { get; set; }
        public bool? Sex { get; set; }
        public bool? Pregnant { get; set; }
        [StringLength(50)]
        public string Username { get; set; }
        [StringLength(10)]
        public string Password { get; set; }
        public int PositionId { get; set; }
        public int DepartmentId { get; set; }
        public long CurrentWage { get; set; }
        public DateTime CreationTime { get; set; }
        public string Token { get; set; }
    }
}