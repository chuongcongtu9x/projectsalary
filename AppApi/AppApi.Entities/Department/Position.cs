﻿namespace AppApi.Entities.Department
{
    public class Position
    {
        public int Id { get; set; }
        public string PositionName { get; set; }
        public long Wage { get; set; }
    }
}
