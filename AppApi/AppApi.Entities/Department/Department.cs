﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AppApi.Entities.Department
{
    public class Department
    {
        public int Id { get; set; }
        [StringLength(50)]
        public string DepartmentName { get; set; }
        public DateTime CreationTime { get; set; }
        public bool IsDelete { get; set; }
    }
}
