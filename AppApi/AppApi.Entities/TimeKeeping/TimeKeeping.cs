﻿using System;

namespace AppApi.Entities.TimeKeeping
{
    public class TimeKeeping
    {
        public int Id { get; set; }
        public int EmpId { get; set; }
        public float WorkDay { get; set; }
        public int DayOff { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime? ModifyDate { get; set; }
        public bool IsDelete { get; set; }
    }
}
