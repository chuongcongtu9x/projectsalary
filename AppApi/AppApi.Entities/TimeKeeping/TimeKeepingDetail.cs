﻿using System;

namespace AppApi.Entities.TimeKeeping
{
    public class TimeKeepingDetail
    {
        public int Id { get; set; }
        public int TimeKeepingId { get; set; }
        public double Work { get; set; }
        public string Note { get; set; }
        public DateTime? WorkDate { get; set; }
        public DateTime? OffDate { get; set; }
        public DateTime CreationTime { get; set; }
        public DateTime? ModifyDate { get; set; }
        public bool IsDelete { get; set; }
    }
}
