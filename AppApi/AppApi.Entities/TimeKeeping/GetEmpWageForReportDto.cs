﻿using System.Collections.Generic;

namespace AppApi.Entities.TimeKeeping
{
    public class GetEmpWageForReportDto
    {
        public GetTimeKeepingForEmp Employee { get; set; }
        public List<TimeKeepingDetail> TimeKeepings { get; set; }
    }
}
