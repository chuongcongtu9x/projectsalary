﻿using System;

namespace AppApi.Entities.TimeKeeping
{
    public class DayOffRequestInputDto
    {
        public int EmpId { get; set; }
        public DateTime OffDate { get; set; }
        public string Note { get; set; }
    }
}
