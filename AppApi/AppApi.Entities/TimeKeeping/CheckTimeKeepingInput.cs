﻿namespace AppApi.Entities.TimeKeeping
{
    public class CheckTimeKeepingInput
    {
        public int EmpId { get; set; }
        public double WorkTime { get; set; }
        public bool Pregnant { get; set; }
    }
}
