﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AppApi.Entities.TimeKeeping
{
    public class GetTimeKeepingForEmp
    {
        public int TimeKeepingId { get; set; }
        public int EmpId { get; set; }
        public double? WorkDay { get; set; }
        public double? MonthWage { get; set; }
        [StringLength(50)]
        public string EmpName { get; set; }
        public DateTime Birthday { get; set; }
        public bool? Sex { get; set; }
        public bool? Pregnant { get; set; }
        [StringLength(50)]
        public string Position { get; set; }
        [StringLength(50)]
        public string Department { get; set; }
    }
}
