﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppApi.Entities.TimeKeeping
{
    public class TimeKeepingInputDto
    {
        public string EmpName { get; set; }
        public int? EmpId { get; set; }
        public int? EmpTypeId { get; set; }
        public DateTime? Month { get; set; }
        public int? PositionId { get; set; }
    }
}
