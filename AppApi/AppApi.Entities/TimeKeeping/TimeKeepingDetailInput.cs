﻿using System;

namespace AppApi.Entities.TimeKeeping
{
    public class TimeKeepingDetailInput
    {
        public int TimeKeepingId { get; set; }
        public DateTime? Month { get; set; }
    }
}
