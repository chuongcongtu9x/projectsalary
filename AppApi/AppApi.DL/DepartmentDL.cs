﻿using AppApi.Entities.Department;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppApi.DL
{
    public class DepartmentDL : DBConnect
    {
        #region -- Get Position list
        public List<Position> GetPositions()
        {
            _conn.Open();
            string sql = string.Format("select * from dbo.Position");

            SqlCommand sqlCommand = new SqlCommand(sql, _conn);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            var positions = new List<Position>();
            if (sqlDataReader.HasRows)
            {
                while (sqlDataReader.Read())
                {
                    var position = new Position();
                    position.Id = (int)sqlDataReader["Id"];
                    position.PositionName = (string)sqlDataReader["Position"];
                    position.Wage = (long)sqlDataReader["Wage"];

                    positions.Add(position);
                }
            }
            _conn.Close();
            return positions.ToList();
        }
        #endregion
        #region -- Get Department List
        public List<Department> GetDepartments()
        {
            _conn.Open();
            string sql = string.Format("select * from dbo.Department");

            SqlCommand sqlCommand = new SqlCommand(sql, _conn);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            var departments = new List<Department>();
            if (sqlDataReader.HasRows)
            {
                while (sqlDataReader.Read())
                {
                    var department = new Department();
                    department.Id = (int)sqlDataReader["Id"];
                    department.DepartmentName = (string)sqlDataReader["Department"];
                    department.IsDelete = (bool)sqlDataReader["IsDelete"];

                    departments.Add(department);
                }
            }
            _conn.Close();
            return departments.ToList();
        }
        #endregion

        #region -- Create Department
        public bool CreateDepartment(Department input)
        {
            _conn.Open();

            string SQL = string.Format("INSERT INTO dbo.Department(Department, CreationTime) VALUES(@Department, @CreationTime)");

            SqlCommand sqlCommand = new SqlCommand(SQL, _conn);
            sqlCommand.Parameters.AddWithValue("@Department", input.DepartmentName);
            sqlCommand.Parameters.AddWithValue("@CreationTime", DateTime.Now);

            if (sqlCommand.ExecuteNonQuery() > 0) return true;
            _conn.Close();
            return false;
        }
        #endregion

        #region -- Update Department
        public bool UpdateDepartment(Department input)
        {
            _conn.Open();

            string SQL = string.Format("UPDATE dbo.Department SET Department = @Department, ModifyDate = @ModifyDate, IsDelete = @IsDelete WHERE Id = @Id");

            SqlCommand sqlCommand = new SqlCommand(SQL, _conn);
            sqlCommand.Parameters.AddWithValue("@Department", input.DepartmentName);
            sqlCommand.Parameters.AddWithValue("@ModifyDate", DateTime.Now);
            sqlCommand.Parameters.AddWithValue("@Id", input.Id);
            sqlCommand.Parameters.AddWithValue("@IsDelete", input.IsDelete);

            if (sqlCommand.ExecuteNonQuery() > 0) return true;
            _conn.Close();
            return false;
        }
        #endregion

        #region --Delete Department
        public bool DeleteDepartment(int id)
        {
            _conn.Open();

            string SQL = string.Format("UPDATE dbo.Department SET IsDelete = 1 WHERE Id = @Id");

            SqlCommand sqlCommand = new SqlCommand(SQL, _conn);
            sqlCommand.Parameters.AddWithValue("@Id", id);

            if (sqlCommand.ExecuteNonQuery() > 0) return true;
            _conn.Close();
            return false;
        }
        #endregion
    }
}
