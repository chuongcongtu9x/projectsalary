﻿using AppApi.Entities;
using AppApi.Entities.Department;
using AppApi.Entities.Employee;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace AppApi.DL
{
    public class EmployeeDL : DBConnect
    {
        #region -- Get
        public List<GetAllEmployeeDto> GetEmployees(EmployeeInputDto input)
        {
            _conn.Open();

            string spName = @"dbo.[sp_GetEmployees]";
            SqlCommand cmd = new SqlCommand(spName, _conn);

            cmd.Parameters.AddWithValue("@EmpId", input.EmpId);
            cmd.Parameters.AddWithValue("@EmpName", input.EmpName ?? "");
            cmd.Parameters.AddWithValue("@DEPARTMENTID", input.DepartmentId);
            cmd.Parameters.AddWithValue("@Sex", input.Sex);
            cmd.Parameters.AddWithValue("@Pregnant", input.Pregnant);

            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader sqlDataReader = cmd.ExecuteReader();

            var employees = new List<GetAllEmployeeDto>();
            if (sqlDataReader.HasRows)
            {
                while (sqlDataReader.Read())
                {
                    var employee = new GetAllEmployeeDto();
                    employee.Id = (int)sqlDataReader["Id"];
                    employee.EmpName = sqlDataReader["EmpName"].ToString();
                    employee.Username = sqlDataReader["Username"].ToString();
                    employee.Password = sqlDataReader["Password"].ToString();
                    employee.Birthday = (DateTime)sqlDataReader["Birthday"];
                    employee.PositionId = (int)sqlDataReader["PositionId"];
                    employee.DepartmentId = (int)sqlDataReader["DepartmentId"];
                    employee.Sex = (bool)sqlDataReader["Sex"];
                    employee.Pregnant = (bool)sqlDataReader["Pregnant"];
                    employee.Position = sqlDataReader["Position"].ToString();
                    employee.Department = sqlDataReader["Department"].ToString();
                    employee.CurrentWage = (long)sqlDataReader["CURRENTWAGE"];

                    employees.Add(employee);
                }
            }
            _conn.Close();
            return employees.ToList();
        }
        #endregion
        #region -- Register
        public bool RegisterDL(Employee input)
        {
            _conn.Open();
            string check = string.Format("select * from Employee where Username = @Username and isDelete = 0");
            SqlCommand sqlCmd = new SqlCommand(check, _conn);
            sqlCmd.Parameters.AddWithValue("@Username", input.Username);
            SqlDataReader sqlDataReader = sqlCmd.ExecuteReader();
            if (sqlDataReader.HasRows)
            {
                _conn.Close();
                return false;
            }
            _conn.Close();
            _conn.Open();

            input.CreationTime = DateTime.Now;
            string SQL = string.Format("INSERT INTO dbo.Employee(Username, Password, EmpName, Birthday, Sex, Pregnant, PositionId, DepartmentId, CurrentWage, CreationTime)"
                        + " VALUES(@Username, @Password, @EmpName, @Birthday, @Sex, @Pregnant, @PositionId, @DepartmentId, @CurrentWage, @CreationTime)");
            SqlCommand sqlCommand = new SqlCommand(SQL, _conn);
            sqlCommand.Parameters.AddWithValue("@Username", input.Username);
            sqlCommand.Parameters.AddWithValue("@Password", input.Password);
            sqlCommand.Parameters.AddWithValue("@EmpName", input.EmpName);
            sqlCommand.Parameters.AddWithValue("@Birthday", input.Birthday.Value.AddDays(1));
            sqlCommand.Parameters.AddWithValue("@Sex", input.Sex);
            sqlCommand.Parameters.AddWithValue("@Pregnant", input.Pregnant);
            sqlCommand.Parameters.AddWithValue("@CurrentWage", input.CurrentWage);
            sqlCommand.Parameters.AddWithValue("@PositionId", input.PositionId);
            sqlCommand.Parameters.AddWithValue("@DepartmentId", input.DepartmentId);
            sqlCommand.Parameters.AddWithValue("@CreationTime", input.CreationTime);

            if (sqlCommand.ExecuteNonQuery() > 0) return true;
            _conn.Close();
            return false;
        }
        #endregion
        #region -- Update
        public bool UpdateDL(Employee input)
        {
            switch (input.PositionId)
            {
                case 2:
                    input.CurrentWage = 500000;
                    break;
                case 3:
                    input.CurrentWage = 700000;
                    break;
                default:
                    input.CurrentWage = 300000;
                    break;
            }

            _conn.Open();

            string SQL = string.Format("UPDATE dbo.Employee SET EmpName = @EmpName, PositionId = @PositionId, DepartmentId= @DepartmentId,"
                    + "Birthday = @Birthday, Sex = @Sex, Pregnant = @Pregnant, ModifyDate = @ModifyDate, CurrentWage = @CurrentWage WHERE Id = @Id");

            SqlCommand sqlCommand = new SqlCommand(SQL, _conn);
            sqlCommand.Parameters.AddWithValue("@EmpName", input.EmpName);
            sqlCommand.Parameters.AddWithValue("@PositionId", input.PositionId);
            sqlCommand.Parameters.AddWithValue("@DepartmentId", input.DepartmentId);
            sqlCommand.Parameters.AddWithValue("@Birthday", input.Birthday);
            sqlCommand.Parameters.AddWithValue("@Sex", input.Sex);
            sqlCommand.Parameters.AddWithValue("@Pregnant", input.Pregnant);
            sqlCommand.Parameters.AddWithValue("@CurrentWage", input.CurrentWage);
            sqlCommand.Parameters.AddWithValue("@ModifyDate", DateTime.Now);
            sqlCommand.Parameters.AddWithValue("@Id", input.Id);

            if (sqlCommand.ExecuteNonQuery() > 0) return true;
            _conn.Close();
            return false;
        }
        #endregion
        #region --Delete
        public bool DeleteDL(Employee input)
        {
            _conn.Open();

            string SQL = string.Format("UPDATE dbo.Employee SET IsDelete = 1 WHERE Id = @id");

            SqlCommand sqlCommand = new SqlCommand(SQL, _conn);
            sqlCommand.Parameters.AddWithValue("@id", input.Id);
            if (sqlCommand.ExecuteNonQuery() > 0) return true;
            _conn.Close();
            return false;
        }
        #endregion
        #region -- Get Token
        public string GetToken()
        {
            string key = "my_secret_key_12345"; //Secret key which will be used later during validation    
            var issuer = "http://mysite.com";  //normally this will be your site URL    

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            //Create a List of Claims, Keep claims name short    
            var permClaims = new List<Claim>();
            permClaims.Add(new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()));

            //Create Security Token object by giving required parameters    
            var token = new JwtSecurityToken(issuer, //Issure    
                            issuer,  //Audience    
                            permClaims,
                            expires: DateTime.Now.AddDays(1),
                            signingCredentials: credentials);
            var jwt_token = new JwtSecurityTokenHandler().WriteToken(token);
            return jwt_token;
        }
        #endregion
        #region -- Login
        public Employee LoginDL(Employee dep)
        {
            _conn.Open();

            string SQL = string.Format("select * from Employee where Username = @Username and Password = @Password and isDelete = 0");
            SqlCommand sqlCommand = new SqlCommand(SQL, _conn);
            sqlCommand.Parameters.AddWithValue("@Username", dep.Username);
            sqlCommand.Parameters.AddWithValue("@Password", dep.Password);
            SqlDataReader sqlDataReader = sqlCommand.ExecuteReader();

            var employees = new List<Employee>();
            if (sqlDataReader.HasRows)
            {
                while (sqlDataReader.Read())
                {
                    var employee = new Employee();
                    employee.Id = (int)sqlDataReader["Id"];
                    employee.Username = sqlDataReader["UserName"].ToString();
                    employee.Password = sqlDataReader["Password"].ToString();
                    employee.EmpName = sqlDataReader["EmpName"].ToString();
                    employee.Birthday = (DateTime)sqlDataReader["Birthday"];
                    employee.Sex = (bool)sqlDataReader["Sex"];
                    employee.Pregnant = (bool)sqlDataReader["Pregnant"];
                    employee.Token = GetToken();
                    employee.PositionId = (int)sqlDataReader["PositionId"];
                    employee.DepartmentId = (int)sqlDataReader["DepartmentId"];
                    employee.CurrentWage = (long)sqlDataReader["CurrentWage"];
                    employees.Add(employee);
                }

                return employees.ToList()[0];
            }
            _conn.Close();
            return null;
        }
        #endregion

    }
}
