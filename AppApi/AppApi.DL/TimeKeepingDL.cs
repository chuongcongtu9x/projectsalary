﻿using AppApi.Entities.TimeKeeping;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

namespace AppApi.DL
{
    public class TimeKeepingDL : DBConnect
    {
        #region -- Get Time Keeping For Emp
        public List<GetTimeKeepingForEmp> GetTimeKeepingForEmpDL(TimeKeepingInputDto input)
        {
            _conn.Open();

            string spName = @"dbo.[SP_GETTIMEKEEPING]";
            SqlCommand cmd = new SqlCommand(spName, _conn);

            cmd.Parameters.AddWithValue("@EMPID", input.EmpId);
            cmd.Parameters.AddWithValue("@EMPNAME", input.EmpName);
            cmd.Parameters.AddWithValue("@DEPARTMENTID", input.EmpTypeId);
            cmd.Parameters.AddWithValue("@MONTH", input.Month);
            cmd.Parameters.AddWithValue("@POSITIONID", input.PositionId);

            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader sqlDataReader = cmd.ExecuteReader();

            List<GetTimeKeepingForEmp> timeKeepings = new List<GetTimeKeepingForEmp>();
            if (sqlDataReader.HasRows)
            {
                while (sqlDataReader.Read())
                {
                    var totalTime = sqlDataReader.GetOrdinal("TOTALTIME");
                    var monthWage = sqlDataReader.GetOrdinal("MONTHWAGE");
                    GetTimeKeepingForEmp timeKeeping = new GetTimeKeepingForEmp();
                    timeKeeping.TimeKeepingId = (int)sqlDataReader["TIMEKEEPINGID"];
                    timeKeeping.WorkDay = sqlDataReader.IsDBNull(totalTime) ? (double?)null : (double?)sqlDataReader.GetDouble(totalTime);
                    timeKeeping.MonthWage = sqlDataReader.IsDBNull(monthWage) ? (double?)null : (double?)sqlDataReader.GetDouble(monthWage);
                    timeKeeping.EmpId = (int)sqlDataReader["EMPID"];
                    timeKeeping.EmpName = sqlDataReader["EmpName"].ToString();
                    timeKeeping.Birthday = (DateTime)sqlDataReader["Birthday"];
                    timeKeeping.Sex = (bool)sqlDataReader["Sex"];
                    timeKeeping.Pregnant = (bool)sqlDataReader["Pregnant"];
                    timeKeeping.Position = sqlDataReader["Position"].ToString();
                    timeKeeping.Department = sqlDataReader["DEPARTMENT"].ToString();
                    timeKeepings.Add(timeKeeping);
                }
            }
            _conn.Close();
            return timeKeepings;
        }
        #endregion
        #region -- Get time keeping detail
        public List<TimeKeepingDetail> GetTimeKeepingDetailDL(TimeKeepingDetailInput input)
        {
            _conn.Open();

            string spName = @"dbo.[SP_GETTIMEKEEPINGDETAIL]";
            SqlCommand cmd = new SqlCommand(spName, _conn);

            cmd.Parameters.AddWithValue("@TIMEKEEPINGID", input.TimeKeepingId);

            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader sqlDataReader = cmd.ExecuteReader();

            List<TimeKeepingDetail> timeKeepingDetails = new List<TimeKeepingDetail>();
            if (sqlDataReader.HasRows)
            {
                while (sqlDataReader.Read())
                {
                    var col = sqlDataReader.GetOrdinal("OFFDATE");
                    var workDate = sqlDataReader.GetOrdinal("WORKDATE");

                    TimeKeepingDetail timeKeepingDetail = new TimeKeepingDetail();
                    timeKeepingDetail.Id = (int)sqlDataReader["ID"];
                    timeKeepingDetail.WorkDate = sqlDataReader.IsDBNull(workDate) ? (DateTime?)null : (DateTime?)sqlDataReader.GetDateTime(workDate);
                    timeKeepingDetail.OffDate = sqlDataReader.IsDBNull(col) ? (DateTime?)null : (DateTime?)sqlDataReader.GetDateTime(col);
                    timeKeepingDetail.Work = (double)sqlDataReader["WORK"];
                    timeKeepingDetail.Note = sqlDataReader["NOTE"].ToString();
                    timeKeepingDetails.Add(timeKeepingDetail);
                }
            }
            _conn.Close();
            return timeKeepingDetails;
        }
        #endregion
        #region -- Time Keeping Now
        public bool CheckTimeKeepingStartDL(CheckTimeKeepingInput input)
        {
            _conn.Open();
            var nowHour = DateTime.Now;
            string spName = @"dbo.[SP_TIMEKEEPINGFOREMP]";
            SqlCommand cmd = new SqlCommand(spName, _conn);
            cmd.Parameters.AddWithValue("@EMPID", input.EmpId);

            if (input.Pregnant == false)
            {
                var ruleTime = DateTime.ParseExact("08:30", "HH:mm", CultureInfo.InvariantCulture);
                var ruleM1 = DateTime.ParseExact("09:30", "HH:mm", CultureInfo.InvariantCulture);
                var ruleM2 = DateTime.ParseExact("10:00", "HH:mm", CultureInfo.InvariantCulture);
                if (nowHour <= ruleTime) input.WorkTime = 1;
                else if (nowHour >= ruleTime && nowHour < ruleM1) input.WorkTime = 0.8;
                else if (nowHour >= ruleM1 && nowHour <= ruleM2) input.WorkTime = 0.5;
                else input.WorkTime = 0;
                cmd.Parameters.AddWithValue("@WORKTIME", input.WorkTime);
            }
            else
            {
                var ruleTime = DateTime.ParseExact("09:30", "HH:mm", CultureInfo.InvariantCulture);
                var ruleM1 = DateTime.ParseExact("10:30", "HH:mm", CultureInfo.InvariantCulture);
                var ruleM2 = DateTime.ParseExact("12:00", "HH:mm", CultureInfo.InvariantCulture);
                if (nowHour <= ruleTime) input.WorkTime = 1;
                else if (nowHour >= ruleTime && nowHour < ruleM1) input.WorkTime = 0.8;
                else if (nowHour >= ruleM1 && nowHour <= ruleM2) input.WorkTime = 0.5;
                else input.WorkTime = 0;
                cmd.Parameters.AddWithValue("@WORKTIME", input.WorkTime);
            }

            cmd.CommandType = CommandType.StoredProcedure;
            var res = cmd.ExecuteScalar().ToString();
            if (Int16.Parse(res) == 1) return true;
            _conn.Close();
            return false;
        }
        #endregion
        #region -- Get time keeping detail of eml
        public List<TimeKeepingDetail> GetTimeKeepingDetailOfEmpDL(TimeKeepingInputDto input)
        {
            _conn.Open();

            string spName = @"dbo.[SP_GETTIMEKEEPINGOFEMP]";
            SqlCommand cmd = new SqlCommand(spName, _conn);

            cmd.Parameters.AddWithValue("@EMPID", input.EmpId);

            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader sqlDataReader = cmd.ExecuteReader();

            List<TimeKeepingDetail> timeKeepingDetails = new List<TimeKeepingDetail>();
            if (sqlDataReader.HasRows)
            {
                while (sqlDataReader.Read())
                {
                    var col = sqlDataReader.GetOrdinal("OFFDATE");
                    var workDate = sqlDataReader.GetOrdinal("WORKDATE");
                    TimeKeepingDetail timeKeepingDetail = new TimeKeepingDetail();
                    timeKeepingDetail.WorkDate = sqlDataReader.IsDBNull(workDate) ? (DateTime?)null : (DateTime?)sqlDataReader.GetDateTime(workDate);
                    timeKeepingDetail.OffDate = sqlDataReader.IsDBNull(col) ? (DateTime?)null : (DateTime?)sqlDataReader.GetDateTime(col);
                    timeKeepingDetail.Work = (double)sqlDataReader["WORK"];
                    timeKeepingDetail.Note = sqlDataReader["NOTE"].ToString();
                    timeKeepingDetails.Add(timeKeepingDetail);
                }
            }
            _conn.Close();
            return timeKeepingDetails;
        }
        #endregion

        #region -- Day Off Request
        public bool DayOffRequestOfEmp(DayOffRequestInputDto input)
        {
            _conn.Open();
            string spName = @"dbo.[SP_DAYOFFREQUEST]";
            SqlCommand cmd = new SqlCommand(spName, _conn);
            cmd.Parameters.AddWithValue("@EMPID", input.EmpId);
            cmd.Parameters.AddWithValue("@OFFDATE", input.OffDate);
            cmd.Parameters.AddWithValue("@NOTE", input.Note);

            cmd.CommandType = CommandType.StoredProcedure;
            var res = cmd.ExecuteScalar().ToString();
            if (Int16.Parse(res) == 1) return true;
            _conn.Close();
            return false;
        }
        #endregion

        #region -- Get Report Emp Wage
        public GetEmpWageForReportDto GetEmpWageForReportDL(TimeKeepingInputDto input)
        {
            GetEmpWageForReportDto empWage = new GetEmpWageForReportDto();
            _conn.Open();

            string spName = @"dbo.[SP_GETTIMEKEEPING]";
            SqlCommand cmd = new SqlCommand(spName, _conn);

            cmd.Parameters.AddWithValue("@EMPID", input.EmpId);
            cmd.Parameters.AddWithValue("@EMPNAME", input.EmpName);
            cmd.Parameters.AddWithValue("@DEPARTMENTID", input.EmpTypeId);
            cmd.Parameters.AddWithValue("@MONTH", input.Month);
            cmd.Parameters.AddWithValue("@POSITIONID", input.PositionId);

            cmd.CommandType = CommandType.StoredProcedure;
            SqlDataReader sqlDataReader = cmd.ExecuteReader();
            List<GetTimeKeepingForEmp> timeKeepings = new List<GetTimeKeepingForEmp>();
            if (sqlDataReader.HasRows)
            {
                while (sqlDataReader.Read())
                {
                    var totalTime = sqlDataReader.GetOrdinal("TOTALTIME");
                    var monthWage = sqlDataReader.GetOrdinal("MONTHWAGE");
                    GetTimeKeepingForEmp timeKeeping = new GetTimeKeepingForEmp();
                    timeKeeping.TimeKeepingId = (int)sqlDataReader["TIMEKEEPINGID"];
                    timeKeeping.WorkDay = sqlDataReader.IsDBNull(totalTime) ? (double?)null : (double?)sqlDataReader.GetDouble(totalTime);
                    timeKeeping.MonthWage = sqlDataReader.IsDBNull(monthWage) ? (double?)null : (double?)sqlDataReader.GetDouble(monthWage);
                    timeKeeping.EmpId = (int)sqlDataReader["EMPID"];
                    timeKeeping.EmpName = sqlDataReader["EmpName"].ToString();
                    timeKeeping.Birthday = (DateTime)sqlDataReader["Birthday"];
                    timeKeeping.Sex = (bool)sqlDataReader["Sex"];
                    timeKeeping.Pregnant = (bool)sqlDataReader["Pregnant"];
                    timeKeeping.Position = sqlDataReader["Position"].ToString();
                    timeKeeping.Department = sqlDataReader["DEPARTMENT"].ToString();
                    timeKeepings.Add(timeKeeping);
                }
                empWage.Employee = timeKeepings[0];
            }
            _conn.Close();

            if (empWage.Employee.EmpId != null)
            {
                string spDetail = @"dbo.[SP_GETTIMEKEEPINGOFEMP]";
                _conn.Open();

                SqlCommand command = new SqlCommand(spDetail, _conn);

                command.Parameters.AddWithValue("@EMPID", empWage.Employee.EmpId);

                command.CommandType = CommandType.StoredProcedure;
                SqlDataReader sqlData = command.ExecuteReader();

                List<TimeKeepingDetail> timeKeepingDetails = new List<TimeKeepingDetail>();
                if (sqlData.HasRows)
                {
                    while (sqlData.Read())
                    {
                        var col = sqlData.GetOrdinal("OFFDATE");
                        var workDate = sqlData.GetOrdinal("WORKDATE");
                        TimeKeepingDetail timeKeepingDetail = new TimeKeepingDetail();
                        timeKeepingDetail.WorkDate = sqlData.IsDBNull(workDate) ? (DateTime?)null : (DateTime?)sqlData.GetDateTime(workDate);
                        timeKeepingDetail.OffDate = sqlData.IsDBNull(col) ? (DateTime?)null : (DateTime?)sqlData.GetDateTime(col);
                        timeKeepingDetail.Work = (double)sqlData["WORK"];
                        timeKeepingDetail.Note = sqlData["NOTE"].ToString();
                        timeKeepingDetails.Add(timeKeepingDetail);
                    }
                    empWage.TimeKeepings = timeKeepingDetails;
                }
                _conn.Close();
            }
            return empWage;
        }
        #endregion
    }
}
